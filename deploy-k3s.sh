#!/usr/bin/env bash
set -e
kubectx k3d-bookCluster
k3d image import -c bookCluster ex-book-list-api-go:latest
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-ex-book-list-api-go ex-book/ex-book-helm-chart --version 0.3.2 --dry-run --debug
#helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-ex-book-list-api-go ex-book/ex-book-helm-chart --version 0.3.2 --wait
