package app

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/app/db"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/app/handler"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/config"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)

// App has the mongo database and router instances
type App struct {
	Router *mux.Router
	DB     *pgx.Conn
	RDB    *redis.Client
	Logger *logrus.Logger
	Config *config.Config
}

// ConfigAndRunApp will create and initialize App structure. App factory functions.
func ConfigAndRunApp(config *config.Config, logFormat string, migrate, load bool, wait int, corsEnabled bool) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(ctx, config, logFormat, wait)

	if load && migrate {
		// Drop all prior to create test data
		err := db.DropAllTables(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}
	}

	if migrate {
		// Migrate Database to the latest version
		err := db.MigrateDatabase(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}
	}
	
	if load && migrate {
		// Create Test DATA
		err := db.CreateTestData(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}
	}

	app.Run(fmt.Sprintf("%s:%s", config.Server.Host, config.Server.Port), corsEnabled)
}

// ConfigAndLoadTestData will configure and initialize core App structure and then only Load Test data.
func ConfigAndLoadTestData(config *config.Config, logFormat string) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(ctx, config, logFormat, 0)
	defer app.DB.Close(context.Background())

	// Drop all prior to create test data
	err := db.DropAllTables(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}

	// Migrate Database to the latest version
	err = db.MigrateDatabase(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}

	// Create Test DATA
	err = db.CreateTestData(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}
}

func ConfigAndMigrateDb(config *config.Config, logFormat string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(ctx, config, logFormat, 0)
	defer app.DB.Close(context.Background())

	// Migrate Database to the latest version
	return db.MigrateDatabase(ctx, app.DB, app.Logger)
}

// Initialize the app with config
func (app *App) Initialize(ctx context.Context, config *config.Config, logFormat string, wait int) {
	// Configuration
	app.Config = config

	// Configure Logger
	app.setLogger(config, logFormat)

	// Configure MongoDB
	app.setPostgresDb(ctx, config, wait)

	// Router
	app.Router = mux.NewRouter()
	app.UseMiddleware(handler.JSONContentTypeMiddleware)
	app.setRouters()
}

func (app *App) setLogger(config *config.Config, format string) {
	app.Logger = logrus.New()
	app.Logger.SetOutput(os.Stdout)
	if strings.ToLower(format) == "json" {
		app.Logger.SetFormatter(&logrus.JSONFormatter{})
		//app.Logger.SetReportCaller(true)
	} else {
		app.Logger.SetFormatter(&logrus.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}

	lvl, err := logrus.ParseLevel(config.Logger.Level)
	if err != nil {
		lvl = logrus.WarnLevel
		app.Logger.SetLevel(lvl)
		app.Logger.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Warning("Configuration Error")
	} else {
		app.Logger.SetLevel(lvl)
	}
}

func (app *App) setPostgresDb(ctx context.Context, c *config.Config, wait int) {
	connConfig, err := pgx.ParseConfig(c.Postgres.Url)
	if err != nil {
		app.Logger.Fatal(err)
	}
	app.waitForConnect(connConfig.Host, strconv.Itoa(int(connConfig.Port)), wait)
	app.DB, err = pgx.ConnectConfig(ctx, connConfig)
	if err != nil {
		app.Logger.Fatal(err)
	}
}

// waitForConnect this is important for docker-compose based work as hostname resolving error was occurring
func (app *App) waitForConnect(host, port string, wait int) {
	timeout := time.Second * time.Duration(wait)
	conn, err := net.DialTimeout("tcp", net.JoinHostPort(host, port), timeout)
	if err != nil {
		app.Logger.Fatalf("Connection Error: %s", host)
	}
	if conn != nil {
		defer conn.Close()
		app.Logger.Warningf("Opened: %s", net.JoinHostPort(host, port))
	}
}

// UseMiddleware will add global middleware in router
func (app *App) UseMiddleware(middleware mux.MiddlewareFunc) {
	app.Router.Use(middleware)
}

func (app *App) setRouters() {
	c := handler.Controller{
		PDB:    app.DB,
		Logger: app.Logger,
		Config: app.Config,
	}
	app.Get("/v1/health", c.GetHealth)
	app.Post("/v1/books", c.CreateBook)
	app.Patch("/v1/books/{id}", c.UpdateBook)
	app.Put("/v1/books/{id}", c.UpdateBook)
	app.Get("/v1/books/{id}", c.GetBook)
	app.Delete("/v1/books/{id}", c.DeleteBook)
	app.Get("/v1/books", c.GetBooks)
	app.Get("/v1/books", c.GetBooks, "page", "{page}")
	app.Router.NotFoundHandler = http.HandlerFunc(c.NotFound)
}

// Get will register Get method for an endpoint
func (app *App) Get(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("GET").Queries(queries...)
}

// Post will register Post method for an endpoint
func (app *App) Post(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("POST").Queries(queries...)
}

// Put will register Put method for an endpoint
func (app *App) Put(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("PUT").Queries(queries...)
}

// Patch will register Patch method for an endpoint
func (app *App) Patch(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("PATCH").Queries(queries...)
}

// Delete will register Delete method for an endpoint
func (app *App) Delete(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("DELETE").Queries(queries...)
}

// Run will start the http server on host that you pass in. host:<ip:port>
func (app *App) Run(host string, corsEnabled bool) {
	// use signals for shutdown server gracefully.
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)
	go func() {
		if corsEnabled {
			appHandler := cors.New(cors.Options{
				AllowedOrigins:   []string{"*"},
				AllowedHeaders:   []string{"*"},
				AllowCredentials: true,
				Debug:            false,
			}).Handler(app.Router)
			app.Logger.Fatal(http.ListenAndServe(host, appHandler))
		} else {
			app.Logger.Fatal(http.ListenAndServe(host, app.Router))
		}
	}()
	app.Logger.Infof("Server is listning on %s://%s", "http", host)

	sig := <-sigs
	app.Logger.Infoln("Signal: ", sig)

	app.Logger.Infoln("Stopping MongoDB Connection...")
	defer app.DB.Close(context.Background())
}
