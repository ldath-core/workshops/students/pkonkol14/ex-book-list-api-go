package model

// Book is the data structure that we will save and receive.
type Book struct {
	ID     int    `json:"id"`
	Title  string `json:"title"`
	Author string `json:"author"`
	Year   string `json:"year"`
}

type CreatedBook struct {
	ID int `json:"id"`
}
