package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/app/model"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/app/repository"
	"net/http"
	"testing"
	"time"
)

func TestCreateBook(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true})
	e, _ := json.Marshal(model.Book{
		Title:  "Test Book Title",
		Author: "Test Author",
		Year:   "2022",
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.CreateBook)

	// check http created status
	rr := createNewRecorder(r, "POST", "/v1/books", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusCreated, rr)

	c.PDB.Close(ctx)
}

func TestGetBooks(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books", c.GetBooks)

	rr := createNewRecorder(r, "GET", "/v1/books", nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	rr = createNewRecorder(r, "GET", "/v1/books", map[string]string{"page": "0"}, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	c.PDB.Close(ctx)
}

func TestGetBook(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.GetBook)

	var book model.Book
	bookRepo := repository.Repository{
		PDB: c.PDB,
		CTX: ctx,
	}
	book, err := bookRepo.GetBook(int64(1))
	if err != nil {
		if err == pgx.ErrNoRows {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	rr := createNewRecorder(r, "GET", fmt.Sprintf("/v1/books/%d", book.ID), nil, nil)
	testResponseStatusCheck(t, http.StatusOK, rr)

	c.PDB.Close(ctx)
}

func TestUpdateBook(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.UpdateBook)

	var book model.Book
	bookRepo := repository.Repository{
		PDB: c.PDB,
		CTX: ctx,
	}
	book, err := bookRepo.GetBook(int64(1))
	if err != nil {
		if err == pgx.ErrNoRows {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}
	book.Title = "Updated Title"
	book.Author = "Updated Author"

	e, _ := json.Marshal(book)

	rr := createNewRecorder(r, "PUT", fmt.Sprintf("/v1/books/%d", book.ID), nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusAccepted, rr)

	c.PDB.Close(ctx)
}

func TestDeleteBook(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{DropCollection: true, LoadTestData: true})

	r := mux.NewRouter()
	r.HandleFunc("/v1/books/{id}", c.DeleteBook)

	var book model.Book
	bookRepo := repository.Repository{
		PDB: c.PDB,
		CTX: ctx,
	}
	book, err := bookRepo.GetBook(int64(1))
	if err != nil {
		if err == pgx.ErrNoRows {
			t.Error("There is 0 documents in the collection - fix tests")
			return
		}
		t.Error(err)
		return
	}

	rr := createNewRecorder(r, "DELETE", fmt.Sprintf("/v1/books/%d", book.ID), nil, nil)
	testResponseStatusCheck(t, http.StatusAccepted, rr)

	c.PDB.Close(ctx)
}
