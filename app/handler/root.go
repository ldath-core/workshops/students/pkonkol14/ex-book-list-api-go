package handler

import (
	"github.com/jackc/pgx/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/config"
	"net/http"
)

type Controller struct {
	PDB    *pgx.Conn
	Logger *logrus.Logger
	Config *config.Config
}

func (c *Controller) NotFound(res http.ResponseWriter, req *http.Request) {
	c.Logger.Warningf("Not found: %s", req.RequestURI)
	err := ResponseWriter(res, http.StatusNotFound, "Page Not Found", nil)
	if err != nil {
		c.Logger.Error(err)
	}
}
