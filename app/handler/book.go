package handler

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/app/model"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/app/repository"
	"net/http"
	"strconv"
)

// CreateBook will handle the create Book post request
func (c *Controller) CreateBook(res http.ResponseWriter, req *http.Request) {
	var book model.Book
	var createdBook model.CreatedBook
	err := json.NewDecoder(req.Body).Decode(&book)
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, "body json request have issues!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	if book.Author == "" || book.Title == "" || book.Year == "" {
		err = ResponseWriter(res, http.StatusNotAcceptable, "please fill all the fields.", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	bookRepo := repository.Repository{
		PDB: c.PDB,
		CTX: req.Context(),
	}
	createdBook.ID, err = bookRepo.AddBook(book)
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with adding book", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	err = ResponseWriter(res, http.StatusCreated, fmt.Sprintf("book: %d created", createdBook.ID), createdBook)
	if err != nil {
		c.Logger.Error(err)
	}
}

// GetBooks will handle people list get request
func (c *Controller) GetBooks(res http.ResponseWriter, req *http.Request) {
	var bookList []model.Book
	skipString := req.FormValue("skip")
	skip, err := strconv.ParseInt(skipString, 10, 64)
	if err != nil {
		skip = 0
	}
	limitString := req.FormValue("limit")
	limit, err := strconv.ParseInt(limitString, 10, 64)
	if err != nil {
		limit = 10
	}
	bookRepo := repository.Repository{
		PDB: c.PDB,
		CTX: req.Context(),
	}
	count, err := bookRepo.CountBooks()
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with counting books", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	bookList, err = bookRepo.GetBooks(limit, skip)
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with getting books", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	err = PaginatedResponseWriter(res, http.StatusOK, count, skip, limit, fmt.Sprintf("books - skip: %d; limit: %d", skip, limit), bookList)
	if err != nil {
		c.Logger.Error(err)
	}
}

// GetBook will give us book with special id
func (c *Controller) GetBook(res http.ResponseWriter, req *http.Request) {
	var params = mux.Vars(req)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, fmt.Sprintf("id: %s is wrong", params["id"]), nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	bookRepo := repository.Repository{
		PDB: c.PDB,
		CTX: req.Context(),
	}
	book, err := bookRepo.GetBook(int64(id))
	if err != nil {
		if err == pgx.ErrNoRows {
			err = ResponseWriter(res, http.StatusNotFound, "there is no documents with thus id", nil)
		} else {
			err = ResponseWriter(res, http.StatusInternalServerError, "there is an error on server!!!", nil)
		}
		c.Logger.Error(err)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	err = ResponseWriter(res, http.StatusOK, "book", book)
	if err != nil {
		c.Logger.Error(err)
	}
}

// UpdateBook will handle the book update endpoint
func (c *Controller) UpdateBook(res http.ResponseWriter, req *http.Request) {
	var params = mux.Vars(req)
	var book model.Book

	id, err := strconv.Atoi(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, fmt.Sprintf("id: %s is wrong", params["id"]), nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	err = json.NewDecoder(req.Body).Decode(&book)
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, "body json request have issues!!!", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	if book.Author == "" || book.Title == "" || book.Year == "" {
		err = ResponseWriter(res, http.StatusNotAcceptable, "please fill all the fields.", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	book.ID = id

	bookRepo := repository.Repository{
		PDB: c.PDB,
		CTX: req.Context(),
	}
	rowsUpdated, err := bookRepo.UpdateBook(book)
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with updating book", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	if rowsUpdated == 1 {
		err = ResponseWriter(res, http.StatusAccepted, fmt.Sprintf("book: %d updated", book.ID), &book)
	} else {
		err = ResponseWriter(res, http.StatusNotFound, "book not found", nil)
	}
	if err != nil {
		c.Logger.Error(err)
	}
}

func (c *Controller) DeleteBook(res http.ResponseWriter, req *http.Request) {
	var params = mux.Vars(req)

	id, err := strconv.Atoi(params["id"])
	if err != nil {
		err = ResponseWriter(res, http.StatusBadRequest, fmt.Sprintf("id: %s is wrong", params["id"]), nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}

	bookRepo := repository.Repository{
		PDB: c.PDB,
		CTX: req.Context(),
	}
	rowsDeleted, err := bookRepo.RemoveBook(id)
	if err != nil {
		c.Logger.Error(err)
		err = ResponseWriter(res, http.StatusInternalServerError, "Problem with updating book", nil)
		if err != nil {
			c.Logger.Error(err)
		}
		return
	}
	if rowsDeleted == 0 {
		err = ResponseWriter(res, http.StatusNotFound, "book not found", nil)
	} else {
		err = ResponseWriter(res, http.StatusAccepted, fmt.Sprintf("book: %d deleted", id), nil)
	}
	if err != nil {
		c.Logger.Error(err)
	}
}
