package handler

import (
	"context"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/app/model"
	"net/http"
	"time"
)

// GetHealth will handle health get request
func (c *Controller) GetHealth(res http.ResponseWriter, _ *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	// Ping the primary
	p := true
	if err := c.PDB.Ping(ctx); err != nil {
		p = false
	}

	err := ResponseWriter(res, http.StatusOK, "ex-book-list-api-go api health", model.Health{
		Alive:    true,
		Postgres: p,
	})
	if err != nil {
		c.Logger.Error(err)
	}
}
