package db

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/sirupsen/logrus"
)

func MigrateDatabase(ctx context.Context, conn *pgx.Conn, logger *logrus.Logger) error {
	err := migrateTables(ctx, conn, logger)
	if err != nil {
		logger.Error(err)
		return err
	}
	err = migrateIndexes(ctx, conn, logger)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Migration completed")
	return nil
}

func migrateTables(ctx context.Context, conn *pgx.Conn, logger *logrus.Logger) error {
	_, err := conn.Exec(ctx, "CREATE TABLE IF NOT EXISTS public.books (id integer NOT NULL, title character varying, author character varying, year character varying)")
	if err != nil {
		logger.Error(err)
		return err
	}
	_, err = conn.Exec(ctx, "CREATE SEQUENCE IF NOT EXISTS public.books_id_seq AS integer START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;")
	if err != nil {
		logger.Error(err)
		return err
	}
	_, err = conn.Exec(ctx, "ALTER SEQUENCE public.books_id_seq OWNED BY public.books.id")
	if err != nil {
		logger.Error(err)
		return err
	}
	_, err = conn.Exec(ctx, "ALTER TABLE ONLY public.books ALTER COLUMN id SET DEFAULT nextval('public.books_id_seq'::regclass);")
	if err != nil {
		logger.Error(err)
		return err
	}
	return nil
}

func migrateIndexes(ctx context.Context, database *pgx.Conn, logger *logrus.Logger) error {
	return nil
}
