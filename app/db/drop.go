package db

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/sirupsen/logrus"
)

func DropAllTables(ctx context.Context, conn *pgx.Conn, logger *logrus.Logger) error {
	_, err := conn.Exec(ctx, "DROP TABLE IF EXISTS public.books")
	if err != nil {
		logger.Error(err)
		return err
	}
	_, err = conn.Exec(ctx, "DROP SEQUENCE IF EXISTS public.books_id_seq")
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Dropping all tables completed")
	return nil
}
