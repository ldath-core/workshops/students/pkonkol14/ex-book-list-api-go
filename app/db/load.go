package db

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/sirupsen/logrus"
)

func CreateTestData(ctx context.Context, conn *pgx.Conn, logger *logrus.Logger) error {
	rows := [][]interface{}{
		{"Golang is great", "Mr. Great", "2012"},
		{"C++ is greatest", "Mr. C++", "2015"},
		{"C++ is very old", "Mr. Old", "2014"},
	}
	copyCount, err := conn.CopyFrom(
		ctx,
		pgx.Identifier{"books"},
		[]string{"title", "author", "year"},
		pgx.CopyFromRows(rows),
	)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Infof("Loading test data completed: %d records", copyCount)
	return nil
}
