package repository

import (
	"context"
	"github.com/jackc/pgx/v4"
	"gitlab.com/ldath-core/examples/ex-book-list-api-go/app/model"
)

type Repository struct {
	PDB *pgx.Conn
	CTX context.Context
}

func (r Repository) CountBooks() (int64, error) {
	var count int64
	rows, err := r.PDB.Query(r.CTX, "SELECT COUNT(*) as count FROM books")
	if err != nil {
		return 0, err
	}
	rows.Scan(&count)
	return count, nil
}

func (r Repository) GetBooks(limit, offset int64) ([]model.Book, error) {
	var book model.Book
	var books []model.Book
	rows, err := r.PDB.Query(r.CTX, "SELECT * FROM books LIMIT $1 OFFSET $2", limit, offset)

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)
		books = append(books, book)
	}

	if err != nil {
		return []model.Book{}, err
	}

	return books, nil
}

func (r Repository) GetBook(id int64) (model.Book, error) {
	var book model.Book
	rows := r.PDB.QueryRow(r.CTX, "SELECT * FROM books WHERE id=$1", id)
	err := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)

	return book, err
}

func (r Repository) AddBook(book model.Book) (int, error) {
	var bookID int
	err := r.PDB.QueryRow(r.CTX, "INSERT INTO books (title, author, year) VALUES ($1, $2, $3) RETURNING id;",
		book.Title, book.Author, book.Year).Scan(&bookID)

	if err != nil {
		return 0, err
	}

	return bookID, nil
}

func (r Repository) UpdateBook(book model.Book) (int64, error) {
	result, err := r.PDB.Exec(r.CTX, "UPDATE books SET title=$1, author=$2, year=$3 WHERE id=$4 RETURNING id;",
		&book.Title, &book.Author, &book.Year, &book.ID)

	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (r Repository) RemoveBook(id int) (int64, error) {
	result, err := r.PDB.Exec(r.CTX, "DELETE FROM books WHERE id=$1", id)

	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}
