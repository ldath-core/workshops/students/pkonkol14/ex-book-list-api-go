package config

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"testing"
)

func getTestConfig() (*Config, error) {
	cfgFile := os.Getenv("CFG_FILE")
	viper.SetConfigFile(cfgFile)
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return LoadConfig("text")
}

func GetTestSetup(t *testing.T, ctx context.Context) (l *logrus.Logger, d *pgx.Conn, c *Config) {
	var err error
	// Logger
	l = logrus.New()

	// Config
	c, e := getTestConfig()
	if e != nil {
		t.Fatalf("Error: %s", e.Error())
	}

	// Postgres
	d, err = pgx.Connect(ctx, c.Postgres.Url)
	if err != nil {
		t.Fatalf("Failed to connect to the Postres, error: %s", err)
	}

	return l, d, c
}
