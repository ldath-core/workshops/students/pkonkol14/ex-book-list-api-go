Ansiblefootnote:[How Ansible Works https://www.ansible.com/overview/how-ansible-works] is a radically simple IT automation engine we will be using here for a configuration management.

Most common ways to install Ansible is by using steps available here: https://docs.ansible.com/ansible/latest/installation_guide/index.html

Other method which can be used to install ansible is to use brew.

[source,bash]
----
brew install ansible
----
