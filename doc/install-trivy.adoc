Container security scanner trivyfootnote:[Trivy https://aquasecurity.github.io/trivy/] will be used to scan locally our prepared docker containers

We can install it with the help of brew.

```shell
brew install trivy
```
