Docker Enginefootnote:[Docker Engine https://docs.docker.com/engine/] is an open source containerization technology for building and containerizing your applications and will be required in our work.

For *Linux* most common way to get docker is by installing it with steps available here: https://docs.docker.com/engine/install/ - it is also possible to just install Docker Desktop available here: https://docs.docker.com/get-docker/ which have Docker Engine included.

In case of *macOS* you will need to install Docker Desktop available here: https://docs.docker.com/get-docker/ - other methods of having Docker Engine are now unstable.
