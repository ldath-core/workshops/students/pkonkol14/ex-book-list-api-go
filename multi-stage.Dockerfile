FROM golang:latest AS builder
WORKDIR /go/src/gitlab.com/ldath-core/examples/ex-book-list-api-go
ADD . ./
RUN ./build-linux.sh

FROM alpine:latest
RUN mkdir -p "/var/application"
COPY public /var/application/public
COPY --from=builder /go/src/gitlab.com/ldath-core/examples/ex-book-list-api-go/ex-book-list-api-go /bin/ex-book-list-api-go
EXPOSE 8080
ENTRYPOINT ["/bin/ex-book-list-api-go"]
CMD ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080", "-m"]
